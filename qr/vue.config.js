module.exports = {
  devServer: {
    open: true,
    host: "0.0.0.0",
    port: 8080,
    https: false,
    hot: true,
    hotOnly: false,
    proxy: {
      "/api": {
        target: "http://192.168.1.8:7001",
        changeOrigin: true,
        pathReWrite: {
          ["^/api"]: "/api",
        },
      },
    },
  },
};
