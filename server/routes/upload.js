module.exports = (app) => {
  const express = require("express");
  const router = express.Router();
  const multer = require("multer");

  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "uploads/");
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    },
  });

  var upload = multer({ storage: storage });
  router.post("/img", upload.single("file"), async (req, res) => {
    const file = req.file;
    file.url = `http://192.168.1.8:7001/uploads/${file.filename}`;
    res.send(file);
  });
  app.use("/api", router);
};
