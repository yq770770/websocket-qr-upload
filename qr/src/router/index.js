import Vue from "vue";
import VueRouter from "vue-router";
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

Vue.use(VueRouter);

import Client from "../Cilent.vue";
import Mobile from "../Mobile.vue";

const routes = [
  {
    path: "/",
    redirect: "/client",
  },
  {
    path: "/client",
    component: Client,
  },
  {
    path: "/mobile",
    component: Mobile,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
